package com.example.notesapp.ui.viewModel

sealed interface NoteActions {
    data class ShowError(val message: String): NoteActions
    object CloseScreen : NoteActions
}