package com.example.notesapp.ui.fragments

import com.example.notesapp.ui.model.Note

interface NoteListener {

    fun onItemClick(note: Note)

}