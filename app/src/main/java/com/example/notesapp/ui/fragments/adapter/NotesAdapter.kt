package com.example.notesapp.ui.fragments.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.databinding.NoteItemBinding
import com.example.notesapp.ui.model.Note
import com.example.notesapp.ui.fragments.NoteListener

class NotesAdapter(
    private val itemClickListener: NoteListener
) : ListAdapter<Note, NotesAdapter.NoteViewHolder>(asyncCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = NoteItemBinding.inflate(inflater, parent, false)
        return NoteViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class NoteViewHolder(private val binding: NoteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(note: Note) = with(binding) {
            Log.i("abc123", "bind: 123")
            noteTitle.text = note.title
            date.text = note.dateModified
            root.setOnClickListener {
                itemClickListener.onItemClick(note)
            }
        }
    }

    companion object {
        private val asyncCallback
            get() = object : DiffUtil.ItemCallback<Note>() {
                override fun areItemsTheSame(oldItem: Note, newItem: Note) =
                    oldItem.id == newItem.id

                override fun areContentsTheSame(oldItem: Note, newItem: Note) = oldItem == newItem
            }
    }
}