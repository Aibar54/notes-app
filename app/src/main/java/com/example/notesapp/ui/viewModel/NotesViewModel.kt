package com.example.notesapp.ui.viewModel

import androidx.core.os.bundleOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.example.notesapp.db.LocalStorageProvider
import com.example.notesapp.db.NoteEntity
import com.example.notesapp.ui.Constants.ADD_NOTE
import com.example.notesapp.ui.Constants.REMOVE_NOTE
import com.example.notesapp.ui.model.Note
import com.example.notesapp.ui.viewModel.NoteActions.ShowError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Calendar

class NotesViewModel : ViewModel() {

    private val db = LocalStorageProvider.db.noteDao()
    private val fbAnalytics = LocalStorageProvider.fbAnalytics

    private val _actions = MutableLiveData<NoteActions>()
    val actions: LiveData<NoteActions> = _actions

    val list: LiveData<List<Note>> = db.getAll()
        .map { list -> list.map { Note(it.id, it.name, it.content, it.dateModified) } }

    fun createNote(
        title: String?,
        content: String?
    ) = viewModelScope.launch(IO) {
        require(title != null && content != null)

        if (title.isBlank()) {
            _actions.notify(ShowError("Please enter note name"))
        } else {
            val noteEntity = NoteEntity(
                name = title,
                content = content,
                dateModified = Calendar.getInstance().time.toString()
            )
            db.insert(noteEntity)
            _actions.notify(NoteActions.CloseScreen)

            val params = bundleOf(
                "name" to title,
                "content" to content,
            )
            fbAnalytics.logEvent(ADD_NOTE, params)
        }
    }

    fun editNote(
        id: Long?,
        title: String?,
        content: String?
    ) = viewModelScope.launch(IO) {
        require(id != null && title != null && content != null)

        val noteEntity = NoteEntity(
            id = id,
            name = title,
            content = content,
            dateModified = Calendar.getInstance().time.toString()
        )
        db.update(noteEntity)
        _actions.notify(NoteActions.CloseScreen)
    }

    fun deleteNote(id: Long?) = viewModelScope.launch(IO) {
        require(id != null)

        db.delete(id)

        fbAnalytics.logEvent(REMOVE_NOTE, null)
    }

    /** Send and clear message pull in order to avoid duplications */
    private suspend fun MutableLiveData<NoteActions>.notify(message: NoteActions) =
        withContext(Dispatchers.Main) {
            value = message
            value = null
        }

}