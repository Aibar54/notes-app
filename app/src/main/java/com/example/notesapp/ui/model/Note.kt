package com.example.notesapp.ui.model

data class Note(
    val id: Long,
    val title: String,
    val content: String,
    val dateModified: String
)