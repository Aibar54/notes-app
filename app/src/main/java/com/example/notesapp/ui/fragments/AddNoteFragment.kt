package com.example.notesapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.notesapp.databinding.FragmentAddNoteBinding
import com.example.notesapp.ui.viewModel.NoteActions
import com.example.notesapp.ui.viewModel.NotesViewModel
import com.google.android.material.snackbar.Snackbar

class AddNoteFragment : Fragment() {

    private lateinit var binding: FragmentAddNoteBinding
    private val viewModel: NotesViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentAddNoteBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        viewModel.actions.observe(viewLifecycleOwner) { actions ->
            actions?.let { proceedAction(it) }
        }
    }

    private fun setUpView() {
        binding.saveButton.setOnClickListener {
            val title = binding.nameInput.text.toString()
            val content = binding.contentInput.text.toString()
            viewModel.createNote(title, content)
        }
    }

    private fun proceedAction(action: NoteActions) {
        when (action) {
            NoteActions.CloseScreen -> findNavController().navigateUp()
            is NoteActions.ShowError -> Snackbar.make(
                requireView(),
                action.message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}