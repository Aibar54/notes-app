package com.example.notesapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.notesapp.databinding.FragmentNoteDetailsBinding
import com.example.notesapp.db.LocalStorageProvider
import com.example.notesapp.ui.Constants.VIEW_NOTE
import com.example.notesapp.ui.viewModel.NotesViewModel

class NoteDetailsFragment : Fragment() {

    private lateinit var binding: FragmentNoteDetailsBinding
    private val args: NoteDetailsFragmentArgs by navArgs()
    private val viewModel: NotesViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )
    private val fbAnalytics = LocalStorageProvider.fbAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentNoteDetailsBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()

        fbAnalytics.logEvent(VIEW_NOTE, null)
    }

    private fun setUpView() = with(binding) {
        noteTitle.text = args.noteTitle
        content.text = args.content

        editButton.setOnClickListener {
            val action = NoteDetailsFragmentDirections.actionEditNote(args.noteId)
            findNavController().navigate(action)
        }
        deleteButton.setOnClickListener {
            viewModel.deleteNote(args.noteId)
            findNavController().navigateUp()
        }
    }
}