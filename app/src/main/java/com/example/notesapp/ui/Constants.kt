package com.example.notesapp.ui

object Constants {
    const val ADD_NOTE = "add_note"
    const val REMOVE_NOTE = "remove_note"
    const val VIEW_NOTE = "view_note"
}
