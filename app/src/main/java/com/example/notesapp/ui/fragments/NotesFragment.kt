package com.example.notesapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.notesapp.databinding.FragmentNotesBinding
import com.example.notesapp.ui.fragments.adapter.NotesAdapter
import com.example.notesapp.ui.model.Note
import com.example.notesapp.ui.viewModel.NotesViewModel

class NotesFragment : Fragment(), NoteListener {

    private lateinit var binding: FragmentNotesBinding

    private val adapter = NotesAdapter(this as NoteListener)
    private val viewModel: NotesViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentNotesBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    override fun onItemClick(note: Note) {
        val action = NotesFragmentDirections.actionNoteDetails(note.id, note.title, note.content)
        findNavController().navigate(action)
    }

    private fun setUpView() = with(binding) {
        list.adapter = adapter

        button.setOnClickListener {
            findNavController().navigate(NotesFragmentDirections.actionAddNote())
        }

        viewModel.list.observe(requireActivity()) {
            adapter.submitList(it)
        }
    }
}