package com.example.notesapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.notesapp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        val navController = fragment.navController
        val appBarConfiguration = AppBarConfiguration(navController.graph)

        val toolBar = findViewById<Toolbar>(R.id.tool_bar)
        toolBar.setupWithNavController(navController, appBarConfiguration)
    }
}