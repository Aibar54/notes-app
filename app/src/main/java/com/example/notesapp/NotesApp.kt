package com.example.notesapp

import android.app.Application
import com.example.notesapp.db.LocalStorageProvider

class NotesApp : Application() {

    override fun onCreate() {
        super.onCreate()
        LocalStorageProvider.initialize(this)
    }
}