package com.example.notesapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.google.firebase.analytics.FirebaseAnalytics

@Database(
    entities = [
        NoteEntity::class],
    version = 1
)
abstract class AppDataBase : RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object {
        private const val DB_NAME = "notes_app"

        fun create(context: Context) =
            Room.databaseBuilder(context, AppDataBase::class.java, DB_NAME)
                .build()
    }
}

object LocalStorageProvider {
    lateinit var db: AppDataBase
        private set

    lateinit var fbAnalytics: FirebaseAnalytics
        private set

    fun initialize(appContext: Context) {
        db = AppDataBase.create(appContext)
        fbAnalytics = FirebaseAnalytics.getInstance(appContext)
    }
}