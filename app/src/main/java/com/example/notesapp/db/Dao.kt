package com.example.notesapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import androidx.room.Upsert

@Dao
interface NoteDao {

    @Query("SELECT * FROM notes")
    fun getAll(): LiveData<List<NoteEntity>>

    @Insert
    fun insert(note: NoteEntity): Long

    @Update
    fun update(note: NoteEntity)

    @Query("UPDATE notes SET content=:newNote WHERE id =:id")
    fun update(id: Long, newNote: String)

    @Query("DELETE FROM notes WHERE id=:id")
    fun delete(id: Long)
}